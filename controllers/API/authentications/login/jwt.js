const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const jwtSecret = process.env.JWT_SECRET;
const jwtPayload = {
  logged_in: true
};

/** @type {import('../../../../models/instance')} */
const prisma = require(process.env.ROOT_PATH + '/models/instance');

/**
 *
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
async function controller(req, res, next) {
  if(!req.body.username || !req.body.password) {
    return res.json({
      error: true,
      message: 'Username and password are required',
      data: [],
    })
  }

  const user = await prisma.user_game.findFirst({
    where: {
      username: req.body.username,
    }
  }).catch(err => {
    return {
      error: true,
      message: err.message,
      data: [],
    }
  });

  if(user && user.error){
    return res.status(500).json(user)
  }

  const hash = crypto.createHash('sha512').update(req.body.password).digest('hex');

  if(
    !user
    || user == null
    || user.password != hash
  ) {
    return res.json({
      error: true,
      message: 'Username or password is incorrect',
      data: [],
    })
  }

  jwtPayload['is_admin'] = user.is_admin;
  jwtPayload['user_id'] = user.id;
  const data = jwt.sign(jwtPayload, jwtSecret, { expiresIn: '1800s' });

  if(!data){
    return res.status(500).json({
      error: true,
      message: 'Something went wrong',
      data: [],
    })
  }

  return res.json({
    error: false,
    message: 'Login success',
    data: [{
      token: data,
    }]
  })
}

module.exports = controller;
