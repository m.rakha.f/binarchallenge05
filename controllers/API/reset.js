const crypto = require('crypto');
/** @type {import('../../models/instance')} */
const prisma = require(process.env.ROOT_PATH + '/models/instance');

/**
 *
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
async function controller(req, res, next) {
  if(!req.body.otp || !req.body.password) {
    return res.json({
      error: true,
      message: 'Otp code and password are required',
      data: [],
    })
  }

  const registeredUser = await prisma.user_game.findFirst({
    where: {
      otp_reset: req.body.otp,
    }
  }).catch(err => {
    return {
      error: true,
      message: err.message,
      data: [],
    }
  });

  if(registeredUser && registeredUser.error){
    return res.status(500).json(registeredUser)
  }

  if(registeredUser == null) {
    return res.json({
      error: true,
      message: 'Otp code is not valid',
      data: [],
    })
  }

  const hash = crypto.createHash('sha512').update(req.body.password).digest('hex');

  const data = await prisma.user_game.update({
    where: {
      id: registeredUser.id,
    },
    data: {
      password: hash,
      otp_reset: null,
    }
  }).catch(err => {
    return {
      error: true,
      message: err.message,
      data: [],
    }
  });

  if(data && data.error){
    return res.status(500).json(data)
  }

  return res.json({
    error: false,
    message: 'Reset password success',
    data: [data]
  })
}

module.exports = controller;
