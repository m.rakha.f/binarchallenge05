const crypto = require('crypto');
/** @type {import('../../models/instance')} */
const prisma = require(process.env.ROOT_PATH + '/models/instance');

const mailer = require('nodemailer');
const transporter = mailer.createTransport({
  host: 'smtp.gmail.com',
  port: 465,
  secure: true,
  auth: {
    user: process.env.EMAIL_USER,
    pass: process.env.EMAIL_PASS,
  }
});

/**
 *
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
async function controller(req, res, next) {
  if(!req.body.username) {
    return res.json({
      error: true,
      message: 'Username are required',
      data: [],
    })
  }

  const registeredUser = await prisma.user_game.findFirst({
    where: {
      username: req.body.username,
    }
  }).catch(err => {
    return {
      error: true,
      message: err.message,
      data: [],
    }
  });

  if(registeredUser && registeredUser.error){
    return res.status(500).json(registeredUser)
  }

  if(registeredUser == null) {
    return res.json({
      error: true,
      message: 'Username is not registered',
      data: [],
    })
  }

  if(registeredUser.email == null) {
    return res.json({
      error: true,
      message: 'User has no email',
      data: [],
    })
  }

  //random otp with crypto
  const otp = crypto.randomBytes(4).toString('hex');

  const data = await prisma.user_game.update({
    where: {
      id: registeredUser.id,
    },
    data: {
      otp_reset: otp,
    }
  }).catch(err => {
    return {
      error: true,
      message: err.message,
      data: [],
    }
  });

  if(data && data.error){
    return res.status(500).json(data)
  }

  const mailOptions = {
    from: process.env.EMAIL_USER,
    to: registeredUser.email,
    subject: '[' + process.env.APP_NAME + '] Reset Password',
    text: 'Hello,\n\n' +
      'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
      'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
      'http://' + req.headers.host + '/reset/' + otp + '\n\n' +
      'If you did not request this, please ignore this email and your password will remain unchanged.\n'
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return res.json({
        error: true,
        message: error.toString(),
        data: [],
      });
    }

    return res.json({
      error: false,
      message: 'Email sent',
      data: [],
    });
  });
}

module.exports = controller;
