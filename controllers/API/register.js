const crypto = require('crypto');
/** @type {import('../../models/instance')} */
const prisma = require(process.env.ROOT_PATH + '/models/instance');

const mailer = require('nodemailer');
const transporter = mailer.createTransport({
  host: 'smtp.gmail.com',
  port: 465,
  secure: true,
  auth: {
    user: process.env.EMAIL_USER,
    pass: process.env.EMAIL_PASS,
  }
});

/**
 *
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
async function controller(req, res, next) {
  if(!req.body.username || !req.body.password) {
    return res.json({
      error: true,
      message: 'Username and password are required',
      data: [],
    })
  }

  const registeredUser = await prisma.user_game.findMany({
    where: {
      username: req.body.username,
    }
  }).catch(err => {
    return {
      error: true,
      message: err.message,
      data: [],
    }
  });

  if(registeredUser && registeredUser.error){
    return res.status(500).json(registeredUser)
  }

  if(registeredUser.length > 0) {
    return res.json({
      error: true,
      message: 'Username already exists',
      data: [],
    })
  }

  const hash = crypto.createHash('sha512').update(req.body.password).digest('hex');

  const data = await prisma.user_game.create({
    data: {
      username: req.body.username,
      password: hash,
      email: req.body.email ? req.body.email : null,
    }
  }).catch(err => {
    return {
      error: true,
      message: err.message,
      data: [],
    }
  });

  if(data && data.error){
    return res.status(500).json(data)
  }

  if(!data.email) {
    return res.json({
      error: false,
      message: 'Register success',
      data: [data]
    });
  }

  const mailOptions = {
    from: process.env.EMAIL_USER,
    to: data.email,
    subject: '[' + process.env.APP_NAME + '] Welcome',
    text: 'Welcome to ' + process.env.APP_NAME + '!',
  };

  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      return res.json({
        error: true,
        message: error.toString(),
        data: [],
      });
    }

    return res.json({
      error: false,
      message: 'Register success',
      data: [data]
    });
  });
}

module.exports = controller;
