/** @type {import('../../models/instance')} */
const prisma = require(process.env.ROOT_PATH + '/models/instance');

/**
 *
 * @param {import('express').Request} req
 * @param {import('express').Response} res
 * @param {import('express').NextFunction} next
 */
async function controller(req, res, next) {
  if(!req.params.otp) {
    return res.redirect('/forgot');
  }

  const registeredUser = await prisma.user_game.findFirst({
    where: {
      otp_reset: req.params.otp,
    }
  }).catch(err => {
    return {
      error: true,
      message: err.message,
      data: [],
    }
  });

  if(registeredUser && registeredUser.error){
    return res.status(500).send(registeredUser.message)
  }

  if(registeredUser == null) {
    return res.redirect('/forgot');
  }

  return res.render('reset', { title: 'Reset Password', otp: req.params.otp });
}

module.exports = controller;
